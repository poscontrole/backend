﻿using Microsoft.EntityFrameworkCore;

namespace backend.Models
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
        }
        public DbSet<Marca> marca { get; set; }
        public DbSet<Categoria> categoria { get; set; }
        public DbSet<Produto> produto { get; set; }
        public DbSet<Usuario> usuario { get; set; }
    }
}