﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using backend.Models;

using Swashbuckle.AspNetCore.Swagger;

namespace backend.Controllers
{
    [Route("v1/[controller]")]
    // [ApiConventionType(typeof(DefaultApiConventions))]
    [ApiController]
    public class CategoriaController : Controller
    {
        private readonly AppDbContext _context;

        public CategoriaController(AppDbContext context)
        {
            _context = context;    
        }

        /// <summary>
        /// Consulta os itens cadastrados.
        /// </summary>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="404">Not Found</response>
        [ProducesResponseType(200, Type = typeof(Categoria))]
        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Get()
        {
            var categoria = await _context.categoria.ToListAsync();
            return new ObjectResult(categoria);
        }

        /// <summary>
        /// Consulta um item cadastrado pelo Id.
        /// </summary>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="404">Not Found</response>
        [ProducesResponseType(200, Type = typeof(Categoria))]
        [HttpGet("{id}")]
        [Authorize]
        public async Task<IActionResult> GetById(int? id)
        {
            var categoria = await _context.categoria
                .SingleOrDefaultAsync(m => m.Id == id);
            if (categoria == null)
            {
                return NotFound();
            }
            return new ObjectResult(categoria);
        }

        /// <summary>
        /// Cadastra o item.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///        "name": "Pintura"
        ///        "descricao": "Materiais de insumo para pintura"
        ///     }
        ///
        /// </remarks>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="404">Not Found</response>
        [ProducesResponseType(201, Type = typeof(Categoria))]
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Create([FromBody]Categoria categoria)
        {
            if (ModelState.IsValid)
            {
                try{
                    _context.Add(categoria);
                    await _context.SaveChangesAsync();
                    return new ObjectResult(categoria);
                }
                catch
                {
                }
            }
            Response.StatusCode = 400;
            return Content("Favor revisar os dados enviados!");
        }

        /// <summary>
        /// Atualiza um item cadastrado pelo Id.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///        "id": 1,
        ///        "name": "Pintura"
        ///        "descricao": "Materiais de insumo para pintura"
        ///     }
        ///
        /// </remarks>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="404">Not Found</response>
        [ProducesResponseType(201, Type = typeof(Categoria))]
        [HttpPut]
        [Authorize]
        public async Task<IActionResult> Edit([FromBody]Categoria categoria)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(categoria);
                    await _context.SaveChangesAsync();
                    return new ObjectResult(categoria);
                }
                catch
                {
                    var Existe = _context.categoria.Any(e => e.Id == categoria.Id);
                    if (!Existe)
                    {
                        return NotFound();
                    }
                }
            }
            Response.StatusCode = 400;
            return Content("Favor revisar os dados enviados!");
        }

        /// <summary>
        /// Remove um item cadastrado pelo Id.
        /// </summary>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="404">Not Found</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var categoria = await _context.categoria.SingleOrDefaultAsync(m => m.Id == id);
            _context.categoria.Remove(categoria);
            await _context.SaveChangesAsync();
            return new ObjectResult(categoria);
        }
    }
}