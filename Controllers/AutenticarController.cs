﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using backend.Services;
using backend.Models;
using Swashbuckle.AspNetCore.Swagger;

namespace backend.Controllers
{
    [Route("v1/[controller]")]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [ApiController]
    public class AutenticarController : Controller
    {
        private readonly AppDbContext _context;

        public AutenticarController(AppDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Autenticar usuário.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///        "username": "robson.fernandes",
        ///        "password": "123456"
        ///     }
        ///
        /// </remarks>
        /// <response code="400">Bad Request</response>
        /// <response code="404">Not Found</response>
        [ProducesResponseType(201, Type = typeof(Usuario))]
        [HttpPost]
        // [Authorize]
        public async Task<ActionResult<dynamic>> Authenticate([FromBody]Usuario usuario)
        {
            var user = await _context.usuario
                .SingleOrDefaultAsync(m => m.Username == usuario.Username && m.Password == usuario.Password);
            // Verifica se o usuário existe
            if (user == null)
                return NotFound(new { message = "Usuário ou senha inválidos" });

            // Gera o Token
            var token = TokenService.GenerateToken(user);

            // Oculta a senha
            user.Password = "";

            // Retorna os dados
            return new
            {
                user = user,
                token = token
            };
        }
    }
}