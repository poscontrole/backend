﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using backend.Models;

using Swashbuckle.AspNetCore.Swagger;

namespace backend.Controllers
{
    [Route("v1/[controller]")]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [ApiController]
    public class UsuarioController : Controller
    {
        private readonly AppDbContext _context;

        public UsuarioController(AppDbContext context)
        {
            _context = context;    
        }

        /// <summary>
        /// Consulta os itens cadastrados.
        /// </summary>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="404">Not Found</response>
        [ProducesResponseType(200, Type = typeof(Usuario))]
        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Get()
        {
            var usuario = await _context.usuario.ToListAsync();
            return new ObjectResult(usuario);
        }

        /// <summary>
        /// Consulta um item cadastrado pelo Id.
        /// </summary>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="404">Not Found</response>
        [ProducesResponseType(200, Type = typeof(Usuario))]
        [HttpGet("{id}")]
        [Authorize]
        public async Task<IActionResult> GetById(int? id)
        {
            var usuario = await _context.usuario
                .SingleOrDefaultAsync(m => m.Id == id);
            if (usuario == null)
            {
                return NotFound();
            }
            return new ObjectResult(usuario);
        }

        /// <summary>
        /// Cadastra o item.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///        "username": "robson.fernandes",
        ///        "password": "123456",
        ///        "status": true
        ///     }
        ///
        /// </remarks>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="404">Not Found</response>
        [ProducesResponseType(201, Type = typeof(Usuario))]
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Create([FromBody]Usuario usuario)
        {
            if (ModelState.IsValid)
            {
                try{
                    _context.Add(usuario);
                    await _context.SaveChangesAsync();
                    return new ObjectResult(usuario);
                }
                catch
                {
                }
            }
            Response.StatusCode = 400;
            return Content("Favor revisar os dados enviados!");
        }

        /// <summary>
        /// Atualiza um item cadastrado pelo Id.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///        "id": 1,
        ///        "username": "robson.fernandes",
        ///        "password": "123456",
        ///        "status": true
        ///     }
        ///
        /// </remarks>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="404">Not Found</response>
        [ProducesResponseType(201, Type = typeof(Usuario))]
        [HttpPut]
        [Authorize]
        public async Task<IActionResult> Edit([FromBody]Usuario usuario)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(usuario);
                    await _context.SaveChangesAsync();
                    return new ObjectResult(usuario);
                }
                catch
                {
                    var Existe = _context.usuario.Any(e => e.Id == usuario.Id);
                    if (!Existe)
                    {
                        return NotFound();
                    }
                }
            }
            Response.StatusCode = 400;
            return Content("Favor revisar os dados enviados!");

        }

        /// <summary>
        /// Remove um item cadastrado pelo Id.
        /// </summary>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="404">Not Found</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var usuario = await _context.usuario.SingleOrDefaultAsync(m => m.Id == id);
            _context.usuario.Remove(usuario);
            await _context.SaveChangesAsync();
            return new ObjectResult(usuario);
        }
    }
}