﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using backend.Models;

using Swashbuckle.AspNetCore.Swagger;

namespace backend.Controllers
{
    [Route("v1/[controller]")]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [ApiController]
    public class ProdutoController : Controller
    {
        private readonly AppDbContext _context;

        public ProdutoController(AppDbContext context)
        {
            _context = context;    
        }

        /// <summary>
        /// Consulta os itens cadastrados.
        /// </summary>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="404">Not Found</response>
        [ProducesResponseType(200, Type = typeof(Produto))]
        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Get()
        {
            var produto = await _context.produto.ToListAsync();
            return new ObjectResult(produto);
        }

        /// <summary>
        /// Consulta um item cadastrado pelo Id.
        /// </summary>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="404">Not Found</response>
        [ProducesResponseType(200, Type = typeof(Produto))]
        [HttpGet("{id}")]
        [Authorize]
        public async Task<IActionResult> GetById(int? id)
        {
            var produto = await _context.produto
                .SingleOrDefaultAsync(m => m.Id == id);
            if (produto == null)
            {
                return NotFound();
            }
            return new ObjectResult(produto);
        }

        /// <summary>
        /// Cadastra o item.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///        "nome": "Hidraulica",
        ///        "id_marca": "1",
        ///        "id_categoria": "1",
        ///        "preco": 30.00,
        ///        "status": true
        ///     }
        ///
        /// </remarks>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="404">Not Found</response>
        [ProducesResponseType(201, Type = typeof(Produto))]
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Create([FromBody]Produto produto)
        {
            if (ModelState.IsValid)
            {
                try{
                    _context.Add(produto);
                    await _context.SaveChangesAsync();
                    return new ObjectResult(produto);
                }
                catch
                {
                }
            }
            Response.StatusCode = 400;
            return Content("Favor revisar os dados enviados!");
        }

        /// <summary>
        /// Atualiza um item cadastrado pelo Id.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///        "id": 1,
        ///        "nome": "Hidraulica",
        ///        "id_marca": "1",
        ///        "id_categoria": "1",
        ///        "preco": 30.00,
        ///        "status": true
        ///     }
        ///
        /// </remarks>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="404">Not Found</response>
        [ProducesResponseType(201, Type = typeof(Produto))]
        [HttpPut]
        [Authorize]
        public async Task<IActionResult> Edit([FromBody]Produto produto)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(produto);
                    await _context.SaveChangesAsync();
                    return new ObjectResult(produto);
                }
                catch
                {
                    var Existe = _context.produto.Any(e => e.Id == produto.Id);
                    if (!Existe)
                    {
                        return NotFound();
                    }
                }
            }
            Response.StatusCode = 400;
            return Content("Favor revisar os dados enviados!");
        }

        /// <summary>
        /// Remove um item cadastrado pelo Id.
        /// </summary>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="404">Not Found</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var produto = await _context.produto.SingleOrDefaultAsync(m => m.Id == id);
            _context.produto.Remove(produto);
            await _context.SaveChangesAsync();
            return new ObjectResult(produto);
        }
    }
}