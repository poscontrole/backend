﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using backend.Models;

using Swashbuckle.AspNetCore.Swagger;

namespace backend.Controllers
{
    [Route("v1/[controller]")]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [ApiController]
    public class MarcaController : Controller
    {
        private readonly AppDbContext _context;

        public MarcaController(AppDbContext context)
        {
            _context = context;    
        }

        /// <summary>
        /// Consulta os itens cadastrados.
        /// </summary>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="404">Not Found</response>
        [ProducesResponseType(200, Type = typeof(Marca))]
        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Get()
        {
            var marca = await _context.marca.ToListAsync();
            return new ObjectResult(marca);
        }

        /// <summary>
        /// Consulta um item cadastrado pelo Id.
        /// </summary>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="404">Not Found</response>
        [ProducesResponseType(200, Type = typeof(Marca))]
        [HttpGet("{id}")]
        [Authorize]
        public async Task<IActionResult> GetById(int? id)
        {
            var marca = await _context.marca
                .SingleOrDefaultAsync(m => m.Id == id);
            if (marca == null)
            {
                return NotFound();
            }
            return new ObjectResult(marca);
        }

        /// <summary>
        /// Cadastra o item.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///        "name": "Gerdal"
        ///     }
        ///
        /// </remarks>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="404">Not Found</response>
        [ProducesResponseType(201, Type = typeof(Marca))]
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Create([FromBody]Marca marca)
        {
            if (ModelState.IsValid)
            {
                try{
                    _context.Add(marca);
                    await _context.SaveChangesAsync();
                    return new ObjectResult(marca);
                }
                catch
                {
                }
            }
            Response.StatusCode = 400;
            return Content("Favor revisar os dados enviados!");
        }

        /// <summary>
        /// Atualiza um item cadastrado pelo Id.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///        "name": "Coral"
        ///     }
        ///
        /// </remarks>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="404">Not Found</response>
        [ProducesResponseType(201, Type = typeof(Marca))]
        [HttpPut]
        [Authorize]
        public async Task<IActionResult> Edit([FromBody]Marca marca)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(marca);
                    await _context.SaveChangesAsync();
                    return new ObjectResult(marca);
                }
                catch
                {
                    var Existe = _context.marca.Any(e => e.Id == marca.Id);
                    if (!Existe)
                    {
                        return NotFound();
                    }
                }
            }
            Response.StatusCode = 400;
            return Content("Favor revisar os dados enviados!");
        }

        /// <summary>
        /// Remove um item cadastrado pelo Id.
        /// </summary>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="404">Not Found</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var marca = await _context.marca.SingleOrDefaultAsync(m => m.Id == id);
            _context.marca.Remove(marca);
            await _context.SaveChangesAsync();
            return new ObjectResult(marca);
        }

        private bool MarcaExists(int id)
        {
            return _context.marca.Any(e => e.Id == id);
        }
    }
}