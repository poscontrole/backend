# Iniciando com o POS API

## Obtenha a versão mas nova do banco de dados:

Para que este passo sejá executado, se faz necessario a instância do Mysql instalado na maquina.

O backup só contem a estrutura, ante mão não há dados,

[https://gitlab.com/poscontrole/database](https://gitlab.com/poscontrole/database)\

## Iniciando API

Recomendo utilizar o SDK 5.0.1.

A aplicação esta configurada para a string de conexão:\
"Server=localhost;Database=pos;Uid=root;Pwd="

Caso deseje Alterar a stringe de conexão encontra-se neste no arquivo 'appsettings.json' que está na raiz do projeto
No diretório do projeto, você pode rodar:

### `dotnet build`

Após, rode o comando:

### `dotnet run`

## Documentação (Swagger)

Todos as requisições podem ser feitas pela documentação via interface do swagger.

Interface\
[https://localhost:5001/swagger/index.html](https://localhost:5001/swagger/index.html)

JSON\
[https://localhost:5001/swagger/v1/swagger.json](https://localhost:5001/swagger/v1/swagger.json)