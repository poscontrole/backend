﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace backend.Models
{
    public class Usuario
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

	    /// <example>robson.fernandes</example>
        [Required(ErrorMessage = "O username é obrigatório.")]
		[StringLength(100)]
        public string Username { get; set; }

	    /// <example>123456</example>
        [Required(ErrorMessage = "O password é obrigatório.")]
		[StringLength(100)]
        public string Password { get; set; }

	    /// <example>TRUE</example>
        [Required(ErrorMessage = "O status é obrigatório.")]
        public bool Status { get; set; }
    }
}