﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace backend.Models
{
    public class Marca
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

	    /// <example>Gerdal</example>
        [Required(ErrorMessage = "O nome é obrigatório.")]
		[StringLength(100)]
        public string Nome { get; set; }
    }
}
