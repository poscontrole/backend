﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace backend.Models
{
    public class Categoria
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

	    /// <example>Hidraulica</example>
        [Required(ErrorMessage = "A categoria é obrigatória.")]
		[StringLength(100)]
        public string Nome { get; set; }

	    /// <example>Material hidraulico</example>
        [Required(ErrorMessage = "A categoria é obrigatória.")]
		[StringLength(200)]
        public string Descricao { get; set; }
    }
}
