﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using backend.Models;

namespace backend.Models
{
    public class Produto
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

	    /// <example>Hidraulica</example>
        [Required(ErrorMessage = "A nome é obrigatório.")]
		[StringLength(100)]
        public string Nome { get; set; }
        
	    /// <example>1</example>
        [Required(ErrorMessage = "A marca é obrigatória.")]
        [Range(0, 9999)]
        public int Id_marca { get; set; }

	    /// <example>1</example>
        [Required(ErrorMessage = "A categoria é obrigatória.")]
        [Range(0, 9999)]
        public int Id_categoria { get; set; }

	    /// <example>50.00</example>
        [Required(ErrorMessage = "O preço é obrigatório.")]
        public double Preco { get; set; }

	    /// <example>TRUE</example>
        [Required(ErrorMessage = "O status é obrigatório.")]
        public bool Status { get; set; }
        
        [Key]
        [ForeignKey("Id_marca")]
        public Marca marca { get; set; }
        
        [Key]
        [ForeignKey("Id_categoria")]
        public Categoria categoria { get; set; }
    }
}
